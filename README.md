# REST Scheduler

## Requirements

The application is built in [Lumen](https://lumen.laravel.com), but all you
really need to test it out is Docker, though
[Postman](http://www.getpostman.com) is quite useful for testing the API.

* [Docker](https://www.docker.com/get-started)

## Setup

* Edit `docker-compose.yml` to change any of the ports that you may be already
  running on. By default we'll run the web frontend on port 8015 and a MySQL
  server on 33061.
* Run `docker-compose up -d` to build the Docker containers and run them in the
  background.
* Run `composer install` to download and install all of the Composer
  dependencies.
* Run `php artisan migrate` to set up the database tables.
* Run `php artisan db:seed` to fill the database tables with a small amount of
  test data. Several shifts will be set up for users in the next few days. If
  time moves forward (as it tends to), you may need to run
  `php artisan migrate:refresh && php artisan db:seed` to reset up the database
  with fresh data.
* The API is now listening on http://localhost:8015 unless you changed the port
  specified in `docker-compose.yml`.

### Postman Setup (optional)

If you'd like to use Postman to test the resources in the API, you can import
the file `postman_collection.json` into Postman and it will create a new
collection of example scripts. By default, it doesn't show the description of
scripts, so you'll need to select the script in the left bar, the click the
triangle pointing to the request's name above the request's URL.

## Resources

* [/shifts](http://localhost:8015/shifts) - Shifts resource.
* [/summary](http://localhost:8015/summary) - Show a weekly summary of the
  number of hours worked for each calendar week.

## API Keys

The database is seeded with a few API keys to use in an `apikey` header.

* Han Solo (employee) - 5b3a611fcc1e8020edabb105ee1e4d0012625a5c
* Chewbacca (employee) - 83ee05a1f1428e2d0d09f180cface6d33d54d6b3
* Leia (manager) - e71b60237e41f7952cb9077ffea21aec9ed4277c
