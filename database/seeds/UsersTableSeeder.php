<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Han Solo',
            'role' => 'employee',
            'email' => 'han.solo@example.com',
            'apikey' => sha1('Han Solo'),
            'created_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'name' => 'Chewbacca',
            'role' => 'employee',
            'email' => 'chewbacca@example.com',
            'apikey' => sha1('Chewbacca'),
            'created_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'name' => 'Leia',
            'role' => 'manager',
            'email' => 'general.leia@example.com',
            'apikey' => sha1('Leia'),
            'created_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
