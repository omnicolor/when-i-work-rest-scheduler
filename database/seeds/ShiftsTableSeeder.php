<?php

use Illuminate\Database\Seeder;

class ShiftsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shifts')->insert([
            'employee_id' => 1,
            'manager_id' => 3,
            'break' => 1.0,
            'start_time' => date('Y-m-d H:i:s', strtotime('tomorrow 8am')),
            'end_time' => date('Y-m-d H:i:s', strtotime('tomorrow 5pm')),
            'created_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('shifts')->insert([
            'employee_id' => 3,
            'manager_id' => 3,
            'break' => 1.0,
            'start_time' => date('Y-m-d H:i:s', strtotime('tomorrow 8am')),
            'end_time' => date('Y-m-d H:i:s', strtotime('tomorrow 5pm')),
            'created_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('shifts')->insert([
            'employee_id' => 2,
            'manager_id' => 3,
            'break' => 1.0,
            'start_time' => date('Y-m-d H:i:s', strtotime('tomorrow 4pm')),
            'end_time' => date('Y-m-d H:i:s', strtotime('+2 days 1am')),
            'created_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('shifts')->insert([
            'manager_id' => 3,
            'break' => 1.0,
            'start_time' => '2018-08-21 00:00:00',
            'end_time' => '2018-08-21 09:00:00',
            'start_time' => date('Y-m-d H:i:s', strtotime('+2 days midnight')),
            'end_time' => date('Y-m-d H:i:s', strtotime('+2 days 9am')),
            'created_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('shifts')->insert([
            'employee_id' => 1,
            'manager_id' => 3,
            'break' => 1.0,
            'start_time' => '2018-08-21 08:00:00',
            'end_time' => '2018-08-21 17:00:00',
            'start_time' => date('Y-m-d H:i:s', strtotime('+2 days 8am')),
            'end_time' => date('Y-m-d H:i:s', strtotime('+2 days 5pm')),
            'created_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
