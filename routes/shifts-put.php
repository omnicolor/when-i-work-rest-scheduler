<?php
/**
 * PUT /shifts
 */

declare(strict_types=1);

use App\Exceptions\BadRequestException;
use App\Exceptions\ForbiddenException;
use App\Validations;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

return function ($id, Request $request): Response {
    if ('manager' != $request->user()->role) {
        throw new ForbiddenException();
    }

    $results = app('db')->select(
        'SELECT shifts.id '
        . 'FROM shifts '
        . 'WHERE shifts.id = :id',
        [
            'id' => (int)$id,
        ]
    );
    if (empty($results)) {
        throw new NotFoundHttpException();
    }

    $errors = [];
    try {
        $startTime = Validations::requiredDate(
            $request->input('start_time'),
            'start_date'
        );
    } catch (BadRequestException $ex) {
        $errors[] = [
            'status' => 400,
            'title' => 'Bad Request',
            'detail' => $ex->getMessage(),
        ];
    }
    try {
        $endTime = Validations::requiredDate(
            $request->input('end_time'),
            'end_date'
        );
    } catch (BadRequestException $ex) {
        $errors[] = [
            'status' => 400,
            'title' => 'Bad Request',
            'detail' => $ex->getMessage(),
        ];
    }
    $employeeId = $request->input('employee_id');
    if (!Validations::employeeId($employeeId)) {
        $errors[] = [
            'status' => 400,
            'title' => 'Bad Request',
            'detail' => 'Employee does not appear to exist.',
        ];
    }

    if (!empty($errors)) {
        return response($errors, $errors[0]['status']);
    }

    app('db')->update(
        'UPDATE shifts SET '
        . 'break = :break, '
        . 'employee_id = :employee_id, '
        . 'end_time = :end_time, '
        . 'start_time = :start_time, '
        . 'updated_at = :updated_at '
        . 'WHERE id = :id',
        [
            'break' => $request->input('break'),
            'employee_id' => $employeeId,
            'end_time' => $endTime,
            'id' => (int)$id,
            'start_time' => $startTime,
            'updated_at' => date('Y-m-d H:i:s'),
        ]
    );

    return response('', 204);
};
