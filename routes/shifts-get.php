<?php
/**
 * GET /shifts
 */

declare(strict_types=1);

use App\Exceptions\BadRequestException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

return function (Request $request): Response {
    /**
     * Return employee's upcoming shifts.
     * @param int $userId
     * @return array
     */
    function employeeShifts(int $userId): array {
        $results = app('db')->select(
            'SELECT shifts.id, '
            . 'shifts.break, '
            . 'DATE_FORMAT(shifts.start_time, "%Y-%m-%dT%H:%i:00") AS start_time, '
            . 'DATE_FORMAT(shifts.end_time, "%Y-%m-%dT%H:%i:00") AS end_time, '
            . 'employee.id AS employee_id, '
            . 'employee.name AS employee_name, '
            . 'manager.name AS manager_name, '
            . 'manager.email AS manager_email, '
            . 'manager.phone AS manager_phone '
            . 'FROM shifts '
            . 'INNER JOIN users AS manager ON shifts.manager_id = manager.id '
            . 'INNER JOIN users AS employee ON shifts.employee_id = employee.id '
            . 'WHERE shifts.employee_id = :id '
            . 'AND (start_time > NOW() OR end_time > NOW())',
                [
                    'id' => $userId,
                ]
            );

        // Clean up the results.
        foreach ($results as $key => $value) {
            $value->manager = new StdClass();
            $value->manager->name = $value->manager_name;
            $value->manager->email = $value->manager_email;
            $value->manager->phone = $value->manager_phone;
            unset(
                $value->manager_name,
                $value->manager_email,
                $value->manager_phone
            );
            $results[$key] = $value;
        }
        return $results;
    }

    /**
     * Return employee's upcoming shifts with any coworkers.
     * @param int $userId
     * @return array
     */
    function employeeShiftsWithCoworkers(int $userId): array {
        $results = app('db')->select(
            'SELECT mine.id, '
            . 'mine.break, '
            . 'DATE_FORMAT(mine.start_time, "%Y-%m-%dT%H:%i:00") AS start_time, '
            . 'DATE_FORMAT(mine.end_time, "%Y-%m-%dT%H:%i:00") AS end_time, '
            . 'mine.employee_id, '
            . 'employee.name AS employee_name, '
            . 'manager.name AS manager_name, '
            . 'manager.email AS manager_email, '
            . 'manager.phone AS manager_phone, '
            . 'users.name '
            . 'FROM shifts AS mine '
            . 'INNER JOIN users AS manager ON mine.manager_id = manager.id '
            . 'INNER JOIN users AS employee ON mine.employee_id = employee.id '
            . 'LEFT JOIN shifts AS theirs '
            . 'ON (mine.start_time <= theirs.start_time AND mine.end_time >= theirs.start_time) '
            . 'OR (mine.start_time <= theirs.end_time AND mine.end_time >= theirs.end_time) '
            . 'INNER JOIN users ON theirs.employee_id = users.id '
            . 'WHERE mine.employee_id = :id '
            . 'AND theirs.employee_id != mine.employee_id '
            . 'AND theirs.employee_id IS NOT NULL '
            . 'AND (mine.start_time > NOW() OR mine.end_time > NOW()) '
            . 'ORDER BY mine.id',
            [
                'id' => $userId,
            ]
        );
        $shifts = [];
        $currentShift = null;

        // Now roll the coworkers into an array in each shift instead of showing
        // multiple results for them. There's probably a way to do this in one
        // SQL statement without having to stitch them together after.
        foreach ($results as $shift) {
            if ($currentShift && $shift->id == $currentShift->id) {
                // We've already seen this shift, just add the new coworker to
                // the coworkers list.
                $currentShift->coworkers[] = $shift->name;
                continue;
            }

            // We've never seen the next shift.
            if ($currentShift) {
                // Save the previous shift.
                $shifts[] = $currentShift;
            }

            $shift->coworkers = [$shift->name];
            $shift->manager = new StdClass();
            $shift->manager->name = $shift->manager_name;
            $shift->manager->email = $shift->manager_email;
            $shift->manager->phone = $shift->manager_phone;
            unset(
                $shift->manager_name,
                $shift->manager_email,
                $shift->manager_phone,
                $shift->name
            );
            $currentShift = $shift;
        }
        $shifts[] = $currentShift;

        return $shifts;
    }

    /**
     * Return all upcoming shifts, no matter which employee is assigned (if
     * any).
     * @param ?string $start
     * @param ?string $end
     * @return array
     */
    function managerShifts(?string $start, ?string $end): array {
        if (!$start) {
            // No start date given, default to today.
            $start = date('Y-m-d\T00:00:00');
        } else {
            // See if what the request included looks like a date.
            $start = strtotime($start);
            if (!$start) {
                // It does not look like a date.
                $message = sprintf(
                    'Invalid start date, should be formatted like \'%s\'',
                    date('Y-m-d\TH:i:s')
                );
                throw new BadRequestException($message);
            }
            $start = date('Y-m-d H:i:s', $start);
        }
        if (!$end) {
            // No end date given, default to a week from now.
            $end = date('Y-m-d\T23:59:59', strtotime('+1 week'));
        } else {
            // See if what the request included looks like a date.
            $end = strtotime($end);
            if (!$end) {
                // It does not look like a date.
                $message = sprintf(
                    'Invalid end date, should be formatted like \'%s\'',
                    date('Y-m-d\TH:i:s')
                );
                throw new BadRequestException($message);
            }
            $end = date('Y-m-d H:i:s', $end);
        }
        $results = app('db')->select(
            'SELECT shifts.id, '
            . 'shifts.break, '
            . 'DATE_FORMAT(shifts.start_time, "%Y-%m-%dT%H:%i:00") AS start_time, '
            . 'DATE_FORMAT(shifts.end_time, "%Y-%m-%dT%H:%i:00") AS end_time, '
            . 'manager.name AS manager_name, '
            . 'manager.email AS manager_email, '
            . 'manager.phone AS manager_phone, '
            . 'employee.id AS employee_id, '
            . 'employee.name AS employee_name, '
            . 'employee.email AS employee_email, '
            . 'employee.phone AS employee_phone '
            . 'FROM shifts '
            . 'INNER JOIN users AS manager ON shifts.manager_id = manager.id '
            . 'LEFT JOIN users employee ON shifts.employee_id = employee.id '
            . 'WHERE start_time >= :start AND end_time <= :end',
            [
                'end' => $end,
                'start' => $start,
            ]
        );
        // Clean up the results.
        foreach ($results as $key => $value) {
            $value->manager = new StdClass();
            $value->manager->name = $value->manager_name;
            $value->manager->email = $value->manager_email;
            $value->manager->phone = $value->manager_phone;
            $value->employee = new StdClass();
            if ($value->employee_id) {
                $value->employee->id = $value->employee_id;
                $value->employee->name = $value->employee_name;
                $value->employee->email = $value->employee_email;
                $value->employee->phone = $value->employee_phone;
            }
            unset(
                $value->employee_id,
                $value->employee_name,
                $value->employee_email,
                $value->employee_phone,
                $value->manager_name,
                $value->manager_email,
                $value->manager_phone
            );
            $results[$key] = $value;
        }
        return $results;
    }

    $role = $request->user()->role;
    if ('manager' == $role) {
        $results = managerShifts(
            $request->input('start'),
            $request->input('end')
        );
    } elseif ($request->has('coworkers')) {
        $results = employeeShiftsWithCoworkers($request->user()->id);
    } else {
        $results = employeeShifts($request->user()->id);
    }
    return response($results, 200);
};
