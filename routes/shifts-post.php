<?php
/**
 * POST /shifts
 */

declare(strict_types=1);

use App\Validations;
use App\Exceptions\BadRequestException;
use App\Exceptions\ForbiddenException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

return function (Request $request): Response {
    if ('manager' != $request->user()->role) {
        throw new ForbiddenException();
    }

    $errors = [];
    try {
        $startTime = Validations::requiredDate(
            $request->input('start_time'), 'start_time'
        );
    } catch (BadRequestException $ex) {
        $errors[] = [
            'status' => 400,
            'title' => 'Bad Request',
            'detail' => $ex->getMessage(),
        ];
    }
    try {
        $endTime = Validations::requiredDate(
            $request->input('end_time'),
            'end_time'
        );
    } catch (BadRequestException $ex) {
        $errors[] = [
            'status' => 400,
            'title' => 'Bad Request',
            'detail' => $ex->getMessage(),
        ];
    }
    $employeeId = $request->input('employee_id');
    if (!Validations::employeeId($employeeId)) {
        $errors[] = [
            'status' => 400,
            'title' => 'Bad Request',
            'detail' => 'Employee does not appear to exist.',
        ];
    }
    if (!empty($errors)) {
        return response($errors, $errors[0]['status']);
    }

    $shiftId = DB::table('shifts')->insertGetId([
        'break' => $request->input('break'),
        'employee_id' => $employeeId,
        'end_time' => $endTime,
        'manager_id' => $request->user()->id,
        'start_time' => $startTime,
        'created_at' => date('Y-m-d H:i:s'),
    ]);

    $results = app('db')->select(
        'SELECT shifts.id, '
        . 'shifts.break, '
        . 'DATE_FORMAT(shifts.start_time, "%Y-%m-%dT%H:%i:00") AS start_time, '
        . 'DATE_FORMAT(shifts.end_time, "%Y-%m-%dT%H:%i:00") AS end_time, '
        . 'manager.name AS manager_name, '
        . 'manager.email AS manager_email, '
        . 'manager.phone AS manager_phone, '
        . 'employee.id AS employee_id, '
        . 'employee.name AS employee_name, '
        . 'employee.email AS employee_email, '
        . 'employee.phone AS employee_phone '
        . 'FROM shifts '
        . 'INNER JOIN users AS manager ON shifts.manager_id = manager.id '
        . 'LEFT JOIN users employee ON shifts.employee_id = employee.id '
        . 'WHERE shifts.id = :id',
        [
            'id' => $shiftId,
        ]
    );

    // Clean up the result.
    $shift = $results[0];
    $shift->manager = new StdClass();
    $shift->manager->name = $shift->manager_name;
    $shift->manager->email = $shift->manager_email;
    $shift->manager->phone = $shift->manager_phone;
    $shift->employee = new StdClass();
    if ($shift->employee_id) {
        $shift->employee->id = $shift->employee_id;
        $shift->employee->name = $shift->employee_name;
        $shift->employee->email = $shift->employee_email;
        $shift->employee->phone = $shift->employee_phone;
    }
    unset(
        $shift->employee_id,
        $shift->employee_name,
        $shift->employee_email,
        $shift->employee_phone,
        $shift->manager_name,
        $shift->manager_email,
        $shift->manager_phone
    );
    return response((array)$shift, 201);
};
