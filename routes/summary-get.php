<?php
/**
 * Return a summary of hours worked by week.
 *
 * Hours worked is defined as the full hours worked + partial hours worked
 * - break time.
 */
declare(strict_types=1);

use Illuminate\Http\Request;
use Illuminate\Http\Response;

return function (Request $request): Response {
    $results = app('db')->select(
        'SELECT '
        . 'SUM(ROUND('
        . '  HOUR(TIMEDIFF(end_time, start_time)) + '
        . '  MINUTE(TIMEDIFF(end_time, start_time)) / 60 '
        . '  - break'
        . ', 1)) AS hours, '
        . 'WEEK(start_time) AS week '
        . 'FROM shifts '
        . 'WHERE shifts.employee_id = :id '
        . 'GROUP BY week',
        [
            'id' => $request->user()->id,
        ]
    );
    return response($results, 200);
};
