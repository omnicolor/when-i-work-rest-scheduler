<?php
/**
 * Application Routes.
 */
declare(strict_types=1);

$router->get('/', function () {
    return response(file_get_contents('../README.md'), 200);
});

$router->get('/shifts', require 'shifts-get.php');
$router->post('/shifts', require 'shifts-post.php');
$router->put('/shifts/{id}', require 'shifts-put.php');
$router->get('/summary', require 'summary-get.php');
