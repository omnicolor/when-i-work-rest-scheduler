<?php
/**
 * Helper class for validating various things.
 */

declare(strict_types=1);

namespace App;

use App\Exceptions\BadRequestException;

class Validations
{
    /**
     * Validate and return a date from an input string.
     * @param ?string $value Value to validate
     * @param string $fieldName Name to use for the field in the error message
     * @return string Date formatted string
     */
    public static function requiredDate(
        ?string $value, string $fieldName
    ): string {
        if (!$value) {
            throw new BadRequestException(
                sprintf('Parameter %s is required', $fieldName)
            );
        }

        // See if what the request included looks like a date.
        $date = strtotime($value);
        if (!$date) {
            // It does not look like a date.
            $message = sprintf(
                'Invalid %s, should be formatted like \'%s\'',
                $fieldName,
                date('Y-m-d\TH:i:s')
            );
            throw new BadRequestException($message);
        }
        return date('Y-m-d H:i:s', $date);
    }

    /**
     * An empty employee_id is valid (the shift is unassigned), but if we
     * receive an employee_id, validate that the user exists.
     * @param ?integer $id Employee ID to validate
     * @return bool
     */
    public static function employeeId(?int $id): bool {
        if (null == $id) {
            return true;
        }
        $result = app('db')->select(
            'SELECT id '
            . 'FROM users '
            . 'WHERE id = :id',
            [
                'id' => (int)$id
            ]
        );
        return !empty($result);
    }
}
