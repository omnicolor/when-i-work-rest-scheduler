<?php
/**
 * Exception for when the request had something seriously wrong with it: Bad
 * payload data or missing fields and we can't continue processing.
 */

declare(strict_types=1);

namespace App\Exceptions;

use Exception;

class BadRequestException extends Exception {}
