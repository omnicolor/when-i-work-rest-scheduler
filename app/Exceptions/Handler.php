<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof AuthorizationException) {
            $errors = [
                'errors' => [
                    [
                        'status' => 401,
                        'title' => 'Unauthorized',
                        'detail' => 'The requested resource requires '
                        . 'authentication with an apikey header.',
                    ],
                ],
            ];
            return response($errors, 401);
        }
        if ($e instanceof BadRequestException) {
            $errors = [
                'errors' => [
                    [
                        'status' => 400,
                        'title' => 'Bad Request',
                        'detail' => sprintf(
                            'The request was invalid: %s',
                            $e->getMessage()
                        )
                    ],
                ],
            ];
            return response($errors, 401);
        }
        if ($e instanceof ForbiddenException) {
            $errors = [
                'errors' => [
                    [
                        'status' => 403,
                        'title' => 'Forbidden',
                        'detail' => 'The request was successfully '
                        . 'authenticated, but the user is not authorized '
                        . 'for the method and/or resource.',
                    ],
                ],
            ];
            return response($errors, 403);
        }
        if ($e instanceof MethodNotAllowedHttpException) {
            $errors = [
                'errors' => [
                    [
                        'status' => 405,
                        'title' => 'Bad Method',
                        'detail' => sprintf(
                            'The requested method (%s) is not allowed on the '
                            . 'requested resource.',
                            $request->method()
                        ),
                    ],
                ],
            ];
            return response($errors, 405);
        }
        if ($e instanceof NotFoundHttpException) {
            $errors = [
                'errors' => [
                    [
                        'status' => 404,
                        'title' => 'Not Found',
                        'detail' => 'The requested resource was not found.',
                    ],
                ],
            ];
            return response($errors, 404);
        }
        return parent::render($request, $e);
    }
}
