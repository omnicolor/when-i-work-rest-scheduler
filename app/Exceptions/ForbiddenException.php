<?php
/**
 * Exception for when the request was successfully authenticated, but is not
 * authorized for whatever it was the request was trying to do.
 */

declare(strict_types=1);

namespace App\Exceptions;

use Exception;

class ForbiddenException extends Exception {}
