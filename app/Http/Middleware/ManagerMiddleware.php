<?php
declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
Use App\Exceptions\ForbiddenException;

class ManagerMiddleware
{
    /**
     * Handle an incoming request and reject it if the user does not have the
     * Manager role.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->user()) {
            throw new AuthorizationException();
        }
        if ('manager' !== $request->user()->role) {
            throw new ForbiddenException();
        }
        return $next($request);
    }
}
